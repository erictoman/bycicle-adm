$(document).ready(function() {
  limitDate();
  obtenerBicicletas();
  obtenerEscuelas();
  $("#tablaBicis").on("click", ".modBici", function(e) {
    $a = obtenerFila(e);
    var numero = $($a[0]).text();
    var marca = $($a[1]).text();
    var color = $($a[2]).text();
    var estado = $($a[3]).text();
    $("#titleBici").text("Modificar informacion de bicicleta: " + numero);
    $("#idBicicleta").val(numero);
    $("#marca").val(marca);
    $("#color").val(color);
    $("#estado").val(estado);
    $("#TipoUsoBici").val("Modificar");
    $("#RegistroBicicletas").addClass("is-active");
  });

  //Registro bicicletas ocultar
  $("#CGB").on("click", function(e) {
    cerrarRegistrobici();
  });

  $("#AGB").on("click", function(e) {
    if (validaCamposBiciRegistro() == 1) {
      if ($("#TipoUsoBici").val() === "Registro") {
        registroBicicleta();
      } else {
        if ($("#TipoUsoBici").val() === "Modificar") {
          modificarBicicleta();
        }
      }
    } else {
      alert("Falta llenar un campo");
    }
  });

  $("#RegistroBiciButton").on("click", function(e) {
    $("#RegistroBicicletas").addClass("is-active");
    $("#TipoUsoBici").val("Registro");
    $("#titleBici").text("Registrar bicicleta");
  });

  function cerrarRegistrobici() {
    $("#RegistroBicicletas").removeClass("is-active");
    clearPanelBici();
  }

  function validaCamposBiciRegistro() {
    var a = $("#marca").val();
    var b = $("#color").val();
    var c = $("#estado").val();
    if (a === "" || b === "" || c === "") {
      return 0;
    }
    return 1;
  }

  function registroBicicleta() {
    var dataForm = $("#formularioRegistroBici").serialize();
    $.ajax({
      url: "./admin/registroBicicleta",
      type: "POST",
      data: dataForm,
      beforeSend: function(xhr) {
        xhr.withCredentials = true;
      },
      success: result => {
        result = JSON.parse(result);
        if (result == 1) {
          obtenerBicicletas();
          alert("Registro Exitoso");
          cerrarRegistrobici();
          clearPanelBici();
          clearTablaBicisFull();
        } else {
          alert("Registro Fallido contacte soporte");
        }
      }
    });
  }

  function modificarBicicleta() {
    var dataForm =
      $("#formularioRegistroBici").serialize() +
      "&idBicicleta=" +
      $("#idBicicleta").val();
    $.ajax({
      url: "./admin/cambioBicicleta",
      type: "POST",
      data: dataForm,
      beforeSend: function(xhr) {
        xhr.withCredentials = true;
      },
      success: result => {
        console.log(result);
        result = JSON.parse(result);
        if (result == 1) {
          obtenerBicicletas();
          alert("Cambio Exitoso");
          cerrarRegistrobici();
          clearPanelBici();
          clearTablaBicisFull();
        } else {
          alert("Registro Fallido contacte soporte");
        }
      }
    });
  }

  function clearTablaBicisFull() {
    $("#tablaBicis").empty();
  }

  function obtenerBicicletas() {
    $.ajax({
      url: "./admin/Bicis",
      type: "GET",
      beforeSend: function(xhr) {
        xhr.withCredentials = true;
      },
      success: result => {
        for (var bici of result) {
          $("#tablaBicis").append(
            "<tr>" +
              "<td>" +
              bici.idBicicleta +
              "</td>" +
              "<td>" +
              bici.Marca +
              "</td>" +
              "<td>" +
              bici.Color +
              "</td>" +
              "<td>" +
              bici.Estado +
              "</td>" +
              '<td><a class="button is-small is-primary modBici">Modificar</a></td>' +
              "</tr>"
          );
        }
      }
    });
  }

  function obtenerBicicletas() {
    $.ajax({
      url: "./admin/Bicis",
      type: "GET",
      beforeSend: function(xhr) {
        xhr.withCredentials = true;
      },
      success: result => {
        for (var bici of result) {
          $("#tablaBicis").append(
            "<tr>" +
              "<td>" +
              bici.idBicicleta +
              "</td>" +
              "<td>" +
              bici.Marca +
              "</td>" +
              "<td>" +
              bici.Color +
              "</td>" +
              "<td>" +
              bici.Estado +
              "</td>" +
              '<td><a class="button is-small is-primary modBici">Modificar</a></td>' +
              "</tr>"
          );
        }
      }
    });
  }

  function obtenerBicicletasDisponibles() {
    $.ajax({
      url: "./admin/BicisDisp",
      type: "GET",
      beforeSend: function(xhr) {
        xhr.withCredentials = true;
      },
      success: result => {
        console.log(result);
        for (var bici of result) {
          $("#noDeBici").append(
            $("<option></option>")
              .attr("value", bici.idBicicleta)
              .text(
                bici.idBicicleta +
                  "/" +
                  bici.Marca +
                  "/" +
                  bici.Color +
                  "/" +
                  bici.Estado
              )
          );
        }
      }
    });
  }

  function obtenerFila(e) {
    $a = $(e.target);
    $td = $($a.parent()[0]).parent()[0];
    $id = $($td).find("td");
    return $id;
  }

  function clearPanelBici() {
    $("#idBicicleta").val("");
    $("#marca").val("");
    $("#color").val("");
    $("#estado").val("");
  }

  //NUEVAS FUNCIONES

  //Registro e informacion de alumno
  $("#RAlumno").on("click", e => {
    $("#TipoUsoAlumno").val("Registro");
    $("#RegistroAlumno").addClass("is-active");
    $("#AsignaBici").hide();
  });

  $("#FindBol").on("click", e => {
    var boleta = $("#BoletatoFind").val();
    $("#TipoUsoAlumno").val("BuscarAlumno");
    showAlumnoInformation(boleta);
  });

  $("#BGuardarA").on("click", e => {
    if (validaCamposAlumnoRegistro() == 1) {
      if ($("#TipoUsoAlumno").val() === "Registro") {
        registroAlumno();
      } else {
        if ($("#TipoUsoAlumno").val() === "BuscarAlumno") {
          actualizaAlumno();
        }
      }
    } else {
      alert("Falta llenar un campo");
    }
  });

  $("#BCancelA").on("click", e => {
    closeAlumnoModal();
  });
  //Funciones de linkeo de alumno y bicicleta
  $("#AsignaBici").click(function(e) {
    $("#boleta_alumno").val($("#boletaAlumno").val());
    $("#RegistroBiciAlumno").addClass("is-active");
    obtenerBicicletasDisponibles();
  });

  $("#Aceptar_Prestamo").click(async function(e) {
    var idAlumno = $("#boleta_alumno").val();
    var idBici = $("#noDeBici").val();
    await realizarRegistro(idAlumno, idBici);
    closeAlumnoModal();
    $("#RegistroBiciAlumno").removeClass("is-active");
  });

  $("#Cancelar_Prestamo").click(function(e) {
    closeAlumnoModal();
    $("#RegistroBiciAlumno").removeClass("is-active");
  });

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  });

  //Bitacora
  $("#dateoflog").on("change", function(e) {
    console.log($(this).val());
    var fecha = $(this).val();
    solicitarBitacora(fecha);
  });

  $("#table_bitacora").on("click", "#BotonFinalizar", function(e) {
    var id = $(e.target).val();
    if (confirm("Confirmar la entrega de bicicleta")) {
      FinPrestamo(id);
    }
  });

  $("#dateoflog").flatpickr({
    locale: "es", // locale for this instance only
    maxDate: new Date()
  });
});

function closeAlumnoModal() {
  $("#nombreAlumno").prop("readonly", false);
  $("#apAlumno").prop("readonly", false);
  $("#amAlumno").prop("readonly", false);
  $("#boletaAlumno").prop("readonly", false);
  $("#edadAlumno").prop("readonly", false);
  $("#escuelaAlumno").prop("readonly", false);
  $("#escuelaAlumno").prop("disabled", false);
  $("#formularioRegistroAlumno").trigger("reset");
  $("#RegistroAlumno").removeClass("is-active");
  $("#BGuardarA").prop("disabled", false);
}

function obtenerEscuelas() {
  $.ajax({
    url: "./admin/Escuelas",
    type: "GET",
    beforeSend: function(xhr) {
      xhr.withCredentials = true;
    },
    success: result => {
      $("#escuelaAlumno").empty(); //append($("<option></option>").attr("value", 'Hola').text('key'));
      for (var Escuela of result) {
        $("#escuelaAlumno").append(
          $("<option></option>")
            .attr("value", Escuela.idEscuela)
            .text(Escuela.Nombre)
        );
      }
    }
  });
}

function validaCamposAlumnoRegistro() {
  var a = $("#nombreAlumno").val();
  var b = $("#apAlumno").val();
  var c = $("#amAlumno").val();
  var d = $("#edadAlumno").val();
  var e = $("#boletaAlumno").val();
  var f = $("#escuelaAlumno").val();
  if (a === "" || b === "" || c === "" || d === "" || e === "" || f === "") {
    return 0;
  }
  return 1;
}

function registroAlumno() {
  console.log("Se registra usuario");
  var dataForm = $("#formularioRegistroAlumno").serialize();
  console.log(dataForm);
  $.ajax({
    url: "./admin/registroAlumno",
    type: "POST",
    data: dataForm,
    beforeSend: function(xhr) {
      xhr.withCredentials = true;
    },
    success: result => {
      if (result.status) {
        alert("Registro Exitoso");
        closeAlumnoModal();
      } else {
        alert(result.Descripcion);
      }
    }
  });
}

function actualizaAlumno() {
  console.log("Se registra usuario");
  var dataForm = $("#formularioRegistroAlumno").serialize();
  console.log(dataForm);
  $.ajax({
    url: "./admin/actualizaAlumno",
    type: "POST",
    data: dataForm,
    beforeSend: function(xhr) {
      xhr.withCredentials = true;
    },
    success: result => {
      if (result.status) {
        $("#BGuardarA").prop("disabled", true);
        $("#edadAlumno").prop("readonly", true);
        $("#escuelaAlumno").prop("readonly", true);
        $("#escuelaAlumno").prop("disabled", true);
        alert("Actualizacion Exitosa");
      } else {
        alert(result.Descripcion);
      }
    }
  });
}

function showAlumnoInformation(boleta) {
  console.log(boleta);
  getBoletaInformation({
    boleta: boleta
  });
  $("#RegistroAlumno").removeClass("is-active");
}

function getBoletaInformation(Boleta) {
  console.log("Se busca Informacion");
  $.ajax({
    url: "./admin/BuscarBoleta",
    type: "POST",
    data: Boleta,
    beforeSend: function(xhr) {
      xhr.withCredentials = true;
    },
    success: result => {
      console.log(result);
      if (result.status) {
        showAlumnoModalInfo(result.datos);
      } else {
        alert("No se encontro boleta");
      }
    }
  });
}

function showAlumnoModalInfo(datos) {
  $("#nombreAlumno").val(datos.Nombre);
  $("#apAlumno").val(datos.ApellidoP);
  $("#amAlumno").val(datos.ApellidoM);
  if (datos.Edad != undefined) {
    $("#BGuardarA").prop("disabled", true);
    $("#edadAlumno").prop("readonly", true);
  }
  $("#edadAlumno").val(datos.Edad);
  $("#boletaAlumno").val(datos.Boleta);
  console.log(datos);
  if (datos.idEscuela != undefined) {
    $("[name=escuelaAlumno]").val(datos.idEscuela);
    $("#escuelaAlumno").prop("readonly", true);
    $("#escuelaAlumno").prop("disabled", true);
    $("#BGuardarA").prop("disabled", true);
  } else {
    obtenerEscuelas();
    $("#BGuardarA").prop("disabled", false);
  }
  $("#nombreAlumno").prop("readonly", true);
  $("#apAlumno").prop("readonly", true);
  $("#amAlumno").prop("readonly", true);
  $("#boletaAlumno").prop("readonly", true);
  $("#RegistroAlumno").addClass("is-active");
  $("#AsignaBici").show();
}

//Bitacora
function limitDate() {
  var dtToday = new Date();
  var month = dtToday.getMonth() + 1;
  var day = dtToday.getDate();
  var year = dtToday.getFullYear();

  if (month == 1) var minMonth = 12;
  else {
    var minMonth = month - 1;
    if (minMonth < 10) minMonth = "0" + minMonth.toString();
  }
  if (month < 10) month = "0" + month.toString();
  if (day < 10) day = "0" + day.toString();

  var minDate = `${year}-${minMonth}-${day}`;
  var maxDate = year + "-" + month + "-" + day;
  $("#dateoflog").attr("max", maxDate);
  $("#dateoflog").attr("min", minDate);
  $("#dateoflog").attr("value", maxDate);
  solicitarBitacora(maxDate);
}

function solicitarBitacora(fecha) {
  console.log("Solicitar bitacora de " + fecha);
  $.ajax({
    url: "./admin/MostrarBitacora",
    type: "POST",
    data: {
      fecha: fecha
    },
    beforeSend: function(xhr) {
      xhr.withCredentials = true;
    },
    success: result => {
      if (result.status) {
        llenarBitacora(result.datos);
      } else {
        alert("Error en la conexion a la base de datos, contacte a soporte");
      }
    }
  });
}

function llenarBitacora(datos) {
  //Pendiente
  $("#tablelog tbody").empty();
  if (datos.length > 0) {
    for (var dato of datos) {
      var botton;
      if (dato.horaSalida === null) {
        botton =
          "<button id='BotonFinalizar' value='" +
          dato.idRegistro +
          "' class='button is-small is-primary'>Checar salida</button>";
      } else {
        botton = dato.horaSalida;
      }
      $("#table_bitacora").append(
        "<tr><td>" +
          dato.boleta +
          "</td><td>" +
          dato.horaEntrada +
          "</td><td>" +
          botton +
          "</td></tr>"
      );
    }
  }
}

function VaciarBitacora() {
  $("#table_bitacora").empty();
}

//Nuevas funciones
function realizarRegistro(boleta, idbici) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "./admin/RegBiciAlumno",
      type: "POST",
      data: {
        idAlumno: boleta,
        idBici: parseInt(idbici)
      },
      beforeSend: function(xhr) {
        xhr.withCredentials = true;
      },
      success: result => {
        if (result.status) {
          solicitarBitacora($("#dateoflog").val());
        } else {
          alert("Error en la conexion a la base de datos, contacte a soporte");
        }
        resolve();
      }
    });
  });
}

// Funcion para finalizar el prestamo de un registro solo llamar
function FinPrestamo(idRegistro) {
  $.ajax({
    url: "./admin/FinPrestamo",
    type: "POST",
    data: {
      idRegistro: parseInt(idRegistro)
    },
    beforeSend: function(xhr) {
      xhr.withCredentials = true;
    },
    success: result => {
      if (result.status) {
        VaciarBitacora();
        solicitarBitacora($("#dateoflog").val());
      } else {
        alert("Error en la conexion a la base de datos, contacte a soporte");
      }
    }
  });
}
