$(document).ready(() => {
    let enviarData = async function() {
        event.preventDefault();
        var c = $('#formLogin').serialize();
        $('#bLogin').addClass('is-loading');
        a = await sleep(2000);
        $.ajax({
            url: "./login",
            data: c,
            type: 'POST',
            beforeSend: function(xhr){
                xhr.withCredentials = true;
            },
            success: (result) => {
                result = JSON.parse(result)
                console.log(result);
                console.log(typeof(result));
                if (result.status) {
                    window.location.replace(result.redirect);
                } else {
                    alert(`${result.Descripcion}`);
                    $('#bLogin').removeClass('is-loading');
                }
            }
        });
    }

    let sleep = (time) => {
        return new Promise(resolve => {
            setTimeout(() => {
                console.log(`${time} mS`);
                resolve(5 + 7);
            }, time)
        });
    }

    $("#formLogin").validetta({
        showErrorMessages: true,
        bubblePosition: 'bottom',
        bubbleGapLeft: 15, // Right gap of bubble (px unit)
        bubbleGapTop: -5, // Top gap of bubble (px unit)                
        onValid: enviarData
    });
});