const express = require("express");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const bodyParser = require("body-parser");

var { router } = require("./routes/router");

const port = 3000;

console.log("Iniciando servidor");

let app = express();

//Sirve para conocer los valores enviados mediante POST
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//Carpeta que contiene archivos estáticos
app.use(express.static("public"));

//Manejo de cookies
app.use(cookieParser());

//Manejo de sesiones
app.use(
  session({
    secret: "ArkNeo",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  })
);

//Enrutador de la peticiones HTTP realizadas
app.use(router);

app.listen(port, () => {
  console.log(`Servidor iniciado en el puerto: ${port}`);
});
