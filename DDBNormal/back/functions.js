const path = require("path");
const { peticionPost, peticionGet } = require("./peticion");

const loginGet = function(req, res) {
  if (req.session.user === undefined) {
    res.sendFile(path.join(__dirname + `./../pages/login.html`));
  } else {
    res.redirect(`./../admin`);
  }
};

const loginPost = async function(req, res) {
  var { user, password } = req.body;
  var data = await peticionPost({ user, password }, "/login");
  if (data.status) {
    req.session.user = user;
  }
  res.send(JSON.stringify(data));
};

const adminGet = function(req, res) {
  console.log(req.session.user);
  if (req.session.user !== undefined) {
    res.sendFile(path.join(__dirname + `./../pages/main.html`));
  } else {
    res.redirect("/login");
  }
};

const registroAlumno = async function(req, res) {
  if (req.session.user !== undefined) {
    var resultado = await peticionPost(req.body, "/admin/registroAlumno");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

const registroBicicleta = async function(req, res) {
  if (req.session.user !== undefined) {
    var Resultado = await peticionPost(req.body, "/admin/registroBicicleta");
    res.send(JSON.stringify(Resultado));
  }
};

const cambioBicicleta = async function(req, res) {
  if (req.session.user !== undefined) {
    var Resultado = await peticionPost(req.body, "/admin/cambioBicicleta");
    res.send(JSON.stringify(Resultado));
  }
};

const buscarBoleta = async function(req, res) {
  if (req.session.user !== undefined) {
    var { boleta } = req.body; //boleta enviada por formulario
    var resultado = await peticionPost({ boleta }, "/admin/BuscarBoleta");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

const mostrarBitacora = async function(req, res) {
  if (req.session.user !== undefined) {
    var user = req.session.user;
    var { fecha } = req.body; //datos enviados por formulario
    var resultado = await peticionPost(
      { fecha, user },
      "/admin/MostrarBitacora"
    );
    console.log(user + "Bitacora");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

const registrarBiciAlumno = async function(req, res) {
  if (req.session.user !== undefined) {
    var user = req.session.user;
    var { idAlumno, idBici } = req.body; //datos enviados por formulario
    var resultado = await peticionPost(
      { idAlumno, idBici, user },
      "/admin/RegBiciAlumno"
    );
    res.json(resultado);
  }
};

const finPrestamo = async function(req, res) {
  if (req.session.user !== undefined) {
    var { idRegistro } = req.body; //datos enviados por formulario
    var resultado = await peticionPost({ idRegistro }, "/admin/FinPrestamo");
    res.json(resultado);
  }
};

const bicicletasDisponibles = async function(req, res) {
  if (req.session.user !== undefined) {
    var resultado = await peticionGet("/admin/BicisDisp");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

const getBicicletas = async function(req, res) {
  if (req.session.user !== undefined) {
    var resultado = await peticionGet("/admin/Bicis");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

const getEscuelas = async function(req, res) {
  if (req.session.user !== undefined) {
    var resultado = await peticionGet("/admin/Escuelas");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

const actualizaAlumno = async function(req, res) {
  if (req.session.user !== undefined) {
    var resultado = await peticionPost(req.body, "/admin/actualizaAlumno");
    res.json(resultado);
  } else {
    res.redirect("/login");
  }
};

module.exports = {
  actualizaAlumno,
  loginGet,
  loginPost,
  adminGet,
  registroAlumno,
  registroBicicleta,
  cambioBicicleta,
  buscarBoleta,
  mostrarBitacora,
  registrarBiciAlumno,
  bicicletasDisponibles,
  getBicicletas,
  getEscuelas,
  finPrestamo
};
