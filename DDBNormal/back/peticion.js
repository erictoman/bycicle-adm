const fetch = require("node-fetch");

const proxy = "http://localhost:8080";

const peticionPost = async function(jsonObject, ruta) {
  return new Promise(async resolve => {
    var data = await fetch(`${proxy}${ruta}`, {
      method: "post",
      body: JSON.stringify(jsonObject),
      headers: { "Content-Type": "application/json" }
    }).then(res => res.json());
    resolve(data);
  });
};

const peticionGet = async function(ruta) {
  return new Promise(async resolve => {
    var data = await fetch(`${proxy}${ruta}`, {
      method: "get"
    }).then(res => res.json());
    resolve(data);
  });
};

module.exports = {
  peticionPost,
  peticionGet
};
