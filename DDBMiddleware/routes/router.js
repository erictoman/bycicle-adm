const express = require("express");
const functions = require(`../back/functions`);
var router = express.Router();
// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

router.post("/login", (req, res) => {
  functions.loginPost(req, res);
});

router.post("/admin/registroAlumno", (req, res) => {
  functions.registroAlumno(req, res);
});

router.post("/admin/actualizaAlumno", (req, res) => {
  functions.actualizaAlumno(req, res);
});

router.post("/admin/registroBicicleta", (req, res) => {
  functions.registroBicicleta(req, res);
});

router.post("/admin/cambioBicicleta", (req, res) => {
  functions.cambioBicicleta(req, res);
});

router.post("/admin/BuscarBoleta", (req, res) => {
  functions.buscarBoleta(req, res);
});

router.post("/admin/MostrarBitacora", (req, res) => {
  functions.mostrarBitacora(req, res);
});

router.post("/admin/RegBiciAlumno", (req, res) => {
  functions.registrarBiciAlumno(req, res);
});

router.post("/admin/FinPrestamo", (req, res) => {
  functions.finPrestamo(req, res);
});

router.get("/admin/BicisDisp", (req, res) => {
  functions.bicicletasDisponibles(req, res);
});

router.get("/admin/Bicis", (req, res) => {
  functions.getBicicletas(req, res);
});

router.get("/admin/Escuelas", (req, res) => {
  functions.getEscuelas(req, res);
});

module.exports = {
  router
};
