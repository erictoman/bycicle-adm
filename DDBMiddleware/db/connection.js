const mysql = require("mysql");

const databaseBicis = {
  host: "178.128.75.144",
  user: "root",
  password: "password",
  database: "ddb"
};

executeQuery = function(textQuery) {
  return new Promise((resolve, reject) => {
    let connection = mysql.createConnection(databaseBicis);
    connection.query(textQuery, (err, result, fields) => {
      if (err) {
        connection.destroy();
        return reject(err);
      } else {
        resolve({
          result,
          fields
        });
      }
    });
  });
};

module.exports = {
  executeQuery
};
