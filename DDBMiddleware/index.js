const express = require("express");
const bodyParser = require("body-parser");

var { router } = require("./routes/router");

const port = 8080;

console.log("Iniciando servidor");

let app = express();

//Sirve para conocer los valores enviados mediante POST
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//Enrutador de la peticiones HTTP realizadas
app.use(router);

app.listen(port, () => {
  console.log(`Servidor iniciado en el puerto: ${port}`);
});
