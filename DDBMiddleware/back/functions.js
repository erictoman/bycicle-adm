const connection = require(`../db/connection`);

const loginPost = async function(req, res) {
  var { user, password } = req.body;
  try {
    data = await connection.executeQuery(
      `CALL LOGIN('${user}', '${password}')`
    );
    if (data.result[0][0].Resultado == 1) {
      var resultado = { status: true, redirect: "./../admin" };
    } else {
      var resultado = {
        status: false,
        Descripcion: `Error en los datos`
      };
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
  }
  res.json(resultado);
};

const registroAlumno = async function(req, res) {
  var {
    nombreAlumno,
    apAlumno,
    amAlumno,
    boletaAlumno,
    escuelaAlumno,
    edadAlumno
  } = req.body; //datos enviados por formulario
  try {
    data = await connection.executeQuery(
      `CALL RegistroAlumno("${boletaAlumno}", "${nombreAlumno}", "${apAlumno}", "${amAlumno}", ${parseInt(
        edadAlumno
      )}, ${parseInt(escuelaAlumno)})`
    );
    console.log(data.result);
    if (data.result[0][0].Resultado == 1) {
      var resultado = { status: true };
    } else {
      var resultado = {
        status: false,
        Descripcion: `Usuario existente`
      };
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
  }
  res.json(resultado);
};

const registroBicicleta = async function(req, res) {
  var { marca, estado, color } = req.body; //datos enviados por formulario
  var Resultado = 0;
  try {
    data = await connection.executeQuery(
      `CALL RegistrarBicicleta('${marca}','${estado}','${color}')`
    );
    if (data.result[0][0].Resultado == 1) {
      Resultado = 1;
    }
  } catch (error) {
    Resultado = 0;
  }
  res.json(Resultado);
};

const cambioBicicleta = async function(req, res) {
  var { idBicicleta, marca, estado, color } = req.body; //datos enviados por formulario
  var Resultado = 0;
  try {
    data = await connection.executeQuery(
      `CALL modificarBicicleta(${idBicicleta},'${marca}','${estado}','${color}')`
    );
    if (data.result[0][0].Resultado == 1) {
      Resultado = 1;
    }
  } catch (error) {
    Resultado = 0;
  }
  res.json(Resultado);
};

const buscarBoleta = async function(req, res) {
  var { boleta } = req.body; //boleta enviada por formulario
  try {
    data = await connection.executeQuery(`CALL BuscarAlumno( "${boleta}")`);
    if (data.result[0].length == 1) {
      var resultado = {
        status: true,
        datos: data.result[0][0]
      };
    } else {
      var resultado = {
        status: false,
        Descripcion: `No existe el usuario`
      };
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
  }
  res.json(resultado);
};

const mostrarBitacora = async function(req, res) {
  var { fecha, user } = req.body; //datos enviados por formulario
  console.log(user + "Bitacora");
  try {
    data = await connection.executeQuery(
      `CALL Bitacora('${fecha}', '${user}')`
    );
    var resultado = { status: true, datos: data.result[0] };
    res.json(resultado);
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
    res.json(resultado);
  }
};

const registrarBiciAlumno = async function(req, res) {
  var { idAlumno, idBici, user } = req.body; //datos enviados por formulario
  try {
    data = await connection.executeQuery(
      `CALL RegistrarPrestamo('${idAlumno}', ${idBici}, '${user}')`
    );
    var resultado = { status: true };
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
  }
  res.json(resultado);
};

const finPrestamo = async function(req, res) {
  var { idRegistro } = req.body; //datos enviados por formulario
  idRegistro = parseInt(idRegistro);
  try {
    data = await connection.executeQuery(`call FinPrestamo(${idRegistro});`);
    var resultado = { status: true };
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
  }
  res.json(resultado);
};

const bicicletasDisponibles = async function(req, res) {
  try {
    data = await connection.executeQuery(`CALL obtenerBicisDisponibles()`);
    if (data.result[0] !== undefined) {
      res.json(data.result[0]);
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
    res.json(resultado);
  }
};

const getBicicletas = async function(req, res) {
  try {
    data = await connection.executeQuery(`CALL obtenerBicis()`);
    if (data.result[0] !== undefined) {
      res.json(data.result[0]);
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
    res.json(resultado);
  }
};

const getEscuelas = async function(req, res) {
  try {
    data = await connection.executeQuery(`CALL obtenerEscuelas()`);
    if (data.result[0] !== undefined) {
      res.json(data.result[0]);
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
    res.json(resultado);
  }
};

const actualizaAlumno = async function(req, res) {
  var {
    nombreAlumno,
    apAlumno,
    amAlumno,
    boletaAlumno,
    escuelaAlumno,
    edadAlumno
  } = req.body; //datos enviados por formulario
  try {
    data = await connection.executeQuery(
      `CALL ActualizaAlumno("${boletaAlumno}", "${nombreAlumno}", "${apAlumno}", "${amAlumno}", ${parseInt(
        edadAlumno
      )}, ${parseInt(escuelaAlumno)})`
    );
    if (data.result[0][0].Resultado == 1) {
      var resultado = { status: true };
    } else {
      var resultado = {
        status: false,
        Descripcion: `Error al actualizar datos`
      };
    }
  } catch (error) {
    console.log(error);
    var resultado = {
      status: false,
      Descripcion: `Error en la conexión a la base de datos, CODE_ERROR: ${
        error.code
      }`
    };
  }
  res.json(resultado);
};

module.exports = {
  actualizaAlumno,
  loginPost,
  finPrestamo,
  registroAlumno,
  registroBicicleta,
  cambioBicicleta,
  buscarBoleta,
  mostrarBitacora,
  registrarBiciAlumno,
  bicicletasDisponibles,
  getBicicletas,
  getEscuelas
};
